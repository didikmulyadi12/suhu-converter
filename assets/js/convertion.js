function initiate() {
  if (typeof Storage !== "undefined") {
    !localStorage.getItem(HISTORY_KEY) &&
      localStorage.setItem(HISTORY_KEY, JSON.stringify([]));

    localStorage.getItem(HISTORY_KEY) &&
      renderHistory()
  } else {
    alert("Browser Anda tidak mendukung web storage");
  }
}


CONVERT_FORM_ELEMENT.addEventListener('submit', function(e) {
  e.preventDefault();

  const { temperature, fromType, toType } = e.target

  const fromValue = parseFloat(temperature.value)
  let toValue = 0

  if (fromType.value !== toType) {
    if (fromType.value === CELCIUS) {
      if (toType.value === FAHRENHEIT) {
        toValue = celciusToFahrenheit(fromValue)
      }else if(toType.value === REAMUR) {
        toValue = celciusToReamur(fromValue)
      }else if(toType.value === KELVIN) {
        toValue = celciusToKelvin(fromValue)
      }
    }else if(fromType.value === FAHRENHEIT) {
      if (toType.value === CELCIUS) {
        toValue = fahrenheitToCelcius(fromValue)
        console.log(toValue)
      }else if(toType.value === REAMUR) {
        toValue = fahrenheitToReamur(fromValue)
      }else if(toType.value === KELVIN) {
        toValue = fahrenheitToKelvin(fromValue)
      }
    }else if(fromType.value === REAMUR) {
      if (toType.value === CELCIUS) {
        toValue = reamurToCelcius(fromValue)
      }else if(toType.value === FAHRENHEIT) {
        toValue = reamurToFahrenheit(fromValue)
      }else if(toType.value === KELVIN) {
        toValue = reamurToKelvin(fromValue)
      }
    }else if(fromType.value === KELVIN) {
      if (toType.value === CELCIUS) {
        toValue = kelvinToCelcius(fromValue)
      }else if(toType.value === FAHRENHEIT) {
        toValue = kelvinToFahrenheit(fromValue)
      }else if(toType.value === REAMUR) {
        toValue = kelvinToReamur(fromValue)
      }
    }
  }

  toValue = toValue.toFixed(2)

  localStorage.setItem(
    HISTORY_KEY,
    JSON.stringify([
      ...getHistory(),
      {
        id: Date.now(),
        fromValue,
        toValue,
        fromType: fromType.value,
        toType: toType.value,
      }
    ])
  );

  FROM_VALUE_ELEMENT.innerHTML = fromValue
  FROM_TYPE_ELEMENT.innerHTML = fromType.value
  TO_VALUE_ELEMENT.innerHTML = toValue
  TO_TYPE_ELEMENT.innerHTML = toType.value

  renderHistory()
});

initiate();