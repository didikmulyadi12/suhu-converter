const FROM_VALUE_ID = "from-value";
const FROM_TYPE_ID = "from-type";
const TO_VALUE_ID = "to-value";
const TO_TYPE_ID = "to-type";
const TEMPERATURE_INPUT_ID = "temperature-value-input";
const TEMPERATURE_FROM_TYPE_ID = "temperature-from-type-select";
const TEMPERATURE_TO_TYPE_ID = "temperature-to-type-select";
const BTN_CONVERT_ID = "btn-convert";
const CONVERT_FORM_ID = "convert-form";
const HISTORY_LIST_ID = "history-list";

const CELCIUS = "celcius";
const FAHRENHEIT = "fahrenheit";
const KELVIN = "kelvin";
const REAMUR = "reamur";

const HISTORY_KEY = "TEMPERATURECONVERTIONHISTORY"

const FROM_VALUE_ELEMENT = document.querySelector(`#${FROM_VALUE_ID}`);
const FROM_TYPE_ELEMENT = document.querySelector(`#${FROM_TYPE_ID}`);
const TO_VALUE_ELEMENT = document.querySelector(`#${TO_VALUE_ID}`);
const TO_TYPE_ELEMENT = document.querySelector(`#${TO_TYPE_ID}`);
const TEMPERATURE_INPUT_ELEMENT = document.querySelector(`#${TEMPERATURE_INPUT_ID}`);
const TEMPERATURE_FROM_TYPE_ELEMENT = document.querySelector(`#${TEMPERATURE_FROM_TYPE_ID}`);
const TEMPERATURE_TO_TYPE_ELEMENT = document.querySelector(`#${TEMPERATURE_TO_TYPE_ID}`);
const BTN_CONVERT_ELEMENT = document.querySelector(`#${BTN_CONVERT_ID}`);
const CONVERT_FORM_ELEMENT = document.querySelector(`#${CONVERT_FORM_ID}`);
const HISTORY_LIST_ELEMENT = document.querySelector(`#${HISTORY_LIST_ID}`);