const renderHistory = () => {
  const histories = getHistory()
  let historiesHTML = ``
  histories.forEach((history, index) => {
    if (index === 0) {
      historiesHTML += `
        <h2>Histories</h2>
      `
    }

    historiesHTML += `
      <div class="history-item shadow">
        <div class="from-value">${history.fromValue}</div>
        <div class="from-type">${history.fromType}</div>
        <div class="equal">=</div>
        <div class="to-value">${history.toValue}</div>
        <div class="to-type">${history.toType}</div>
      </div>
    `
  })

  HISTORY_LIST_ELEMENT.innerHTML = historiesHTML
}