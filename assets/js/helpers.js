const getHistory = () => {
  console.log(JSON.parse(localStorage.getItem(HISTORY_KEY)))
  return JSON.parse(localStorage.getItem(HISTORY_KEY))
}

// celcius
const celciusToKelvin = (c) => c + 273.15
const celciusToFahrenheit = (c) => (c * 1.8) + 32
const celciusToReamur = (c) => c * 0.8

// kelvin
const kelvinToCelcius = (k) => k - 273.15
const kelvinToFahrenheit = (k) => (k * 1.8) - 459.67
const kelvinToReamur = (k) => celciusToReamur(kelvinToCelcius(k))

// fahrenheit
const fahrenheitToCelcius = (f) => (f - 32) / 1.8
const fahrenheitToKelvin = (f) => (f + 459.67) / 1.8
const fahrenheitToReamur = (f) => (f - 32) * 0.44

// reamur
const reamurToCelcius = (r) => r / 0.8
const reamurToKelvin = (r) => reamurToCelcius(r) + 273.15
const reamurToFahrenheit = (r) => (r * 2.25) + 32